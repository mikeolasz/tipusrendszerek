# Nyelvek típusrendszere tantárgy, 2018. tavasz, ELTE-IK, Programtervező informatikus MSc #

Oktató: [Kaposi Ambrus](https://akaposi.github.io)

Folyamatosan frissített [jegyzet](https://bitbucket.org/akaposi/tipusrendszerek/raw/HEAD/jegyzet.pdf?at=master&fileviewer=file-view-default) ([jegyzet forrása](jegyzet.tex), pull requesteknek örülök).

Ebben a félévben csak vizsgakurzus van, a vizsgákra majd a Neptunban lehet feljelentkezni. [Példa-vizsga](https://bitbucket.org/akaposi/tipusrendszerek/raw/HEAD/peldaVizsga.pdf?at=master&fileviewer=file-view-default).

# Záróvizsga tematika #

Az MSc-s záróvizsga tételsorának S0/4 tétele szól erről a tantárgyról. Korábban ezt a tárgyat Csörnyei Zoltán, Diviánszky Péter és Páli Gábor oktatta, kicsit más tematikával. Az S0/4 tétel jelenleg az új és a régi tananyag metszetét tartalmazza.

Akik a tantárgyat nem nálam végezték, azoknak ideírom, hogy Csörnyei Zoltán könyvében (Bevezetés a típusrendszerek elméletébe, ELTE Eötvös Kiadó, 2012) és Páli Gábor [diáin](http://people.inf.elte.hu/pgj/nytr_msc/) a tételsor anyaga az alábbi módon található meg.

* Absztrakt szintaxisfák, absztrakt kötéses fák, levezetési fák. Csörnyei 2. fejezet. Páli Gábor 1. előadás és 2. diasor a 14. diáig.
* Szintaxis, típusrendszer, operációs szemantika. Ezek bemutathatók például az F1 típusrendszeren. Csörnyei 3.1-3.4. alfejezetek. Páli Gábor 2. diasor, 15-28. dia.
* Típusrendszer és operációs szemantika kapcsolata: haladás és típusmegőrzés tétele. Csörnyei 3.4.22 és 3.4.13 tételek. Páli Gábor 2. diasor, 29. dia.
* Magasabbrendű függvények, Church típusrendszere. Más néven F1 típusrendszer. Csörnyei 3.1-3.4,3.8 alfejezetek. Páli Gábor 2. diasor, 15-30. dia.
* Let kifejezések. Csörnyei 5.2.1. Páli Gábor 6. diasor, 4-5. dia.
* Szorzat és összeg típusok. Csörnyei 3.5.1, 3.5.4, 3.5.5, 3.5.8, 3.5.9. Páli Gábor 3. diasor.
* Induktív típusok: Bool, természetes számok. Csörnyei 3.5.2, 3.5.3, 3.5.7, 3.6. Páli Gábor 3. diasor és 4. diasor a 6. diáig.
* Polimorfizmus (System F), absztrakt típusok. Csörnyei 6. fejezet. Páli Gábor 8. diasor, 9. diasor.
* Altípus. Csörnyei 3.7 fejezet. Páli Gábor 4. diasor 7-18. dia.
* Akik a tárgyat nálam végezték, azoknak a fenti források helyett a jegyzetet ajánlom.

Páli Gábor diái és egy általam tartott gyorstalpaló kurzus táblaképei ebben a repóban is elérhetők.
