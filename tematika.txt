A tantágy tematikája. Ez nem egyezik meg a záróvizsga tematikával.

Absztrakt szintaxisfák, absztrakt kötéses fák, levezetési fák
Szintaxis, típusrendszer, operációs szemantika
Egész számok és sztringek típusrendszere
Típusrendszer és operációs szemantika kapcsolata: haladás és típusmegőrzés tétele
Futási idejű hibák kezelése az operációs szemantikában
Magasabbrendű függvények
Szorzat és összeg típusok
Konstruktív ítéletogika, állítások, mint típusok
Természetes számok és magasabbrendű függvények típusrendszere (System T), definiálható függvények
Generikus programozás, polinomiális és szigorúan pozitív típusoperátorok
Induktív és koinduktív típusok
Polimorfizmus (System F), absztrakt típusok
Altípus
